#
# Makefile template for ATtiny13
# Derived from AVR Crosspack template
#

DEVICE     = attiny13           # See avr-help for all possible devices
CLOCK      = 9600000            # 9.6Mhz
PROGRAMMER = -c stk500v1 -P/dev/ttyACM0
OBJECTS    = main.o             # Add more objects for each .c file here


COMPILE = avr-gcc -Wall -Os -DF_CPU=$(CLOCK) -mmcu=$(DEVICE)

# symbolic targets:
all: main.hex

.c.o:
	$(COMPILE) -c $< -o $@

.S.o:
	$(COMPILE) -x assembler-with-cpp -c $< -o $@

.c.s:
	$(COMPILE) -S $< -o $@

flash: all
	avrdude -v -p$(DEVICE) $(PROGRAMMER) -b19200 -Uflash:w:main.hex:i

clean:
	rm -f main.hex main.elf $(OBJECTS)

# file targets:
main.elf: $(OBJECTS)
	$(COMPILE) -o main.elf $(OBJECTS)

main.hex: main.elf
	rm -f main.hex
	avr-objcopy -j .text -j .data -O ihex main.elf main.hex
	avr-size --format=avr --mcu=$(DEVICE) main.elf
	# If you have an EEPROM section, you must also create a hex file for the
	# EEPROM and add it to the "flash" target.

# Targets for code debugging and analysis:
disasm: main.elf
	avr-objdump -d main.elf

cpp:
	$(COMPILE) -E main.c

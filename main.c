// TODO: use pwm to drive an 8bit christmas melody on a small speaker

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>


#define PIN_LED1 PB0
#define PIN_LED2 PB1
#define PIN_LED3 PB2
#define PIN_BTN1 PB4


#define ON 1


#define MENORAH 0
#define CHRISTMAS 1
#define OFF 2


// write digital "high" to pin <pn> on port <prt>
#define DIGIWRITE_H(prt, pn) prt |= (1<<pn)

// write digital "low" to pin <pn> on port <prt>
#define DIGIWRITE_L(prt, pn) prt &= ~(1<<pn)


// _delay_ms only accepts compile time constants
void long_delay_ms(uint16_t ms) {
    for(ms /= 10; ms>0; ms--) _delay_ms(10);
}


void pwn_enable (void) {
    // Set Timer 0 prescaler to clock/8.
    // At 9.6 MHz this is 1.2 MHz.
    // See ATtiny13 datasheet, Table 11.9.
    TCCR0B |= (1 << CS01);
    // Set to 'Fast PWM' mode
    TCCR0A |= (1 << WGM01) | (1 << WGM00);
    // Clear OC0B output on compare match, upwards counting.
    TCCR0A |= (1 << COM0B1);
}


void pwm_disable (void) {
    TCCR0B = 0;
    TCCR0A = 0;
    OCR0B = 0;
}


void pwm_write (int val) {
    OCR0B = val;
}


void pins_setup() {
    // DDRB is the "data direction register" for port B
    DDRB |= _BV(PIN_LED1) | _BV(PIN_LED2)| _BV(PIN_LED3);

    // BTN1 as input
    DDRB &= ~_BV(PIN_BTN1);
    // with iternal pull-up resistors
    PORTB |= _BV(PIN_BTN1);
}


int button_pressed () {
    if(!(PINB & _BV(PIN_BTN1))) {
        _delay_ms(25);
        if(!(PINB & _BV(PIN_BTN1))) {
            _delay_ms(50);
            return 1;
        }
    }
    return 0;
}


int main(void) {
    pins_setup();

    uint8_t side = MENORAH;
    uint8_t decorations = OFF;
    uint8_t pwm_state = OFF;
    while (1) {

        if(button_pressed()) {
            side = (side + 1) % 3;
            if (pwm_state == ON) {
                pwm_disable();
                pwm_state = OFF;
            }
            DIGIWRITE_L(PORTB, PIN_LED1);
            DIGIWRITE_L(PORTB, PIN_LED2);
            DIGIWRITE_L(PORTB, PIN_LED3);
        }

        switch(side) {
            case MENORAH:
                if (pwm_state == OFF) {
                    pwn_enable();
                    pwm_state = ON;
                }
                pwm_write(rand());
                long_delay_ms(rand() % 512);
                break;
            case CHRISTMAS:
                DIGIWRITE_H(PORTB, PIN_LED1);

                if (decorations == ON) {
                    decorations = OFF;
                    DIGIWRITE_L(PORTB, PIN_LED3);
                } else {
                    decorations = ON;
                    DIGIWRITE_H(PORTB, PIN_LED3);
                }
                _delay_ms(512);
                break;
            case OFF:
                DIGIWRITE_L(PORTB, PIN_LED1);
                DIGIWRITE_L(PORTB, PIN_LED2);
                DIGIWRITE_L(PORTB, PIN_LED3);
                _delay_ms(512);
                break;
        }
    }
    return 0;
}
